const express = require('express');
const router = express();

router.get("/", (req, res) => {
  res.send("welcome!");
})

router.get("/add", (req, res) => {
  try {
    const sum = parseFloat(req.query.a) + parseFloat(req.query.b);
    res.send(sum.toString());
  } catch {
    res.sendStatus(500);
  }
});

module.exports = router;